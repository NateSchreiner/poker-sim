package score

import (
	"gitlab.com/NateSchreiner/poker-sim/card"
	"gitlab.com/NateSchreiner/poker-sim/cardset"
	"gitlab.com/NateSchreiner/poker-sim/hand"
	"gitlab.com/NateSchreiner/poker-sim/statistics"
)

type Result struct {
	Hand  *cardset.CardSet
	Stats *statistics.Stats
	Score hand.Hand
}

// This will simply compute the score of each hand given in ONE ITERATION
// If you want the "real" equity, you must compute this function many times,
// and compute the hand that one the most amount of times
func Compare(hands []*cardset.CardSet) []*Result {
	r := make([]*Result, 0)
	for _, hand := range hands {
		res := &Result{}
		stats := ScoreHand(*hand)
		res.Stats = &stats
		res.Hand = hand
		res.Score = stats.Score
		addHandToReturn(&r, res)
	}

	return r
}

// TODO:: Re-allocating slices here seems really bad
func addHandToReturn(slice *[]*Result, result *Result) {
	for idx, res := range *slice {
		if res.Score <= result.Score {
			n := insert(*slice, idx, result)
			slice = &n
		}
	}
}

func insert(a []*Result, index int, value *Result) []*Result {
	if len(a) == index {
		return append(a, value)
	}

	a = append(a[:index+1], a[index:]...)
	a[index] = value
	return a
}

func CompareHands(first cardset.CardSet, second cardset.CardSet) int {
	firstStats := getBestHand(&first)
	secondStats := getBestHand(&second)

	if firstStats.Score < secondStats.Score {
		return -1
	} else if firstStats.Score == secondStats.Score {
		return 0
	} else {
		return 1
	}
}

func ScoreHand(cs cardset.CardSet) statistics.Stats {
	return getBestHand(&cs)
}

func getBestHand(cs *cardset.CardSet) statistics.Stats {
	suits := make([]card.Suit, 0)
	values := make([]card.Value, 0)
	suitMap := make(map[card.Suit][]*card.Card, 0)
	valMap := make(map[card.Value][]*card.Card, 0)

	iter := cardset.Iter(cs)
	straightDraw := make([]int, 0)
	currStraight := 0
	var last *card.Card = nil

	for iter.More() {
		node := iter.Next()

		if last == nil {
			last = node.Card
		}

		if last.Value-1 == node.Card.Value {
			currStraight++
		} else {
			straightDraw = append(straightDraw, currStraight)
			currStraight = 0
		}

		s := node.Card.Suit
		v := node.Card.Value
		suits = append(suits, s)
		values = append(values, v)

		if _, ok := suitMap[s]; !ok {
			suitMap[s] = []*card.Card{node.Card}
		} else {
			suitMap[s] = append(suitMap[s], node.Card)
		}

		if _, ok := valMap[v]; !ok {
			valMap[v] = []*card.Card{node.Card}
		} else {
			valMap[v] = append(valMap[v], node.Card)
		}
	}
	stats := &statistics.Stats{}
	checkDraws(stats, suitMap, suits, straightDraw)
	stats.Runs = 1

	if has, suit := hasStraightFlush(suitMap, suits); has {
		stats.Score = hand.STRAIGHT_FLUSH
		if len(suitMap[*suit]) >= 5 {
			stats.BestFive = cardset.FromSlice(suitMap[*suit][0:5])
		}
	} else if has, val := hasNum(valMap, values, 4); has {
		stats.Score = hand.FOUR_KIND
		stats.Quads = 1.0
		stats.BestFive = cardset.FromSlice(valMap[*val][0:4])
		iter := cardset.Iter(cs)
		for iter.More() {
			node := iter.Next()
			if !stats.BestFive.Contains(node.Card, false) {
				stats.BestFive.Add(node.Card)
				break
			}
		}
	} else if has, suit = hasFlush(suitMap, suits); has {
		if len(suitMap[*suit]) >= 5 {
			stats.BestFive = cardset.FromSlice(suitMap[*suit][0:5])
		}
		stats.Score = hand.FLUSH
	} else if has, val = hasStraight(valMap, values); has {
		stats.Score = hand.STRAIGHT
		if len(valMap[*val]) >= 5 {
			stats.BestFive = cardset.FromSlice(valMap[*val][0:5])
		}
	} else if has, val = hasNum(valMap, values, 3); has {
		stats.Score = hand.TRIPS
		stats.BestFive = cardset.FromSlice(valMap[*val][0:3])
		iter := cardset.Iter(cs)
		for iter.More() {
			node := iter.Next()
			if !stats.BestFive.Contains(node.Card, false) {
				stats.BestFive.Add(node.Card)
			}
			if stats.BestFive.Len == 5 {
				break
			}
		}
	} else if has, _ = hasTwoPair(valMap, values); has {
		stats.Score = hand.TWO_PAIR
		stats.BestFive = cardset.FromSlice(valMap[*val][0:4])
		iter := cardset.Iter(cs)
		for iter.More() {
			node := iter.Next()
			if !stats.BestFive.Contains(node.Card, false) {
				stats.BestFive.Add(node.Card)
				break
			}
		}
	} else if has, _ = hasNum(valMap, values, 2); has {
		stats.Score = hand.PAIR
		stats.BestFive = cardset.FromSlice(valMap[*val][0:2])
		iter := cardset.Iter(cs)
		for iter.More() {
			node := iter.Next()
			if !stats.BestFive.Contains(node.Card, false) {
				stats.BestFive.Add(node.Card)
			}
			if stats.BestFive.Len == 5 {
				break
			}
		}
	}

	// TODO:: Only high cards

	return *stats
}

func checkDraws(stats *statistics.Stats, suits map[card.Suit][]*card.Card, keys []card.Suit, consecs []int) {
	hasBdf := false
	hasFd := false
	for _, key := range keys {
		if len(suits[key]) == 2 {
			hasBdf = true
		} else if len(suits[key]) == 3 {
			hasFd = true
		}
	}

	if hasBdf && !hasFd {
		stats.BdFlush = 1.0
	} else if hasFd {
		stats.FlushDraw = 1.0
	}
}

func hasStraightFlush(suits map[card.Suit][]*card.Card, suitKeys []card.Suit) (bool, *card.Suit) {

	for _, key := range suitKeys {
		if len(suits[key]) >= 5 {
			has, _ := manualStraightCheck(suits[key])
			if has {
				return true, &key
			}
		}
	}

	return false, nil
}

// Can be used to check for quads, trips, or pair
func hasNum(m map[card.Value][]*card.Card, keys []card.Value, num int) (bool, *card.Value) {
	for _, key := range keys {
		if len(m[key]) >= num {
			return true, &key
		}
	}

	return false, nil
}

func hasFlush(m map[card.Suit][]*card.Card, keys []card.Suit) (bool, *card.Suit) {
	for _, key := range keys {
		if len(m[key]) >= 5 {
			return true, &key
		}
	}

	return false, nil
}

func hasStraight(m map[card.Value][]*card.Card, keys []card.Value) (bool, *card.Value) {
	for _, key := range keys {
		if len(m[key]) >= 5 {
			return true, &key
		}
	}

	return false, nil
}

func hasTwoPair(m map[card.Value][]*card.Card, keys []card.Value) (bool, []card.Value) {
	r := make([]card.Value, 0)
	for _, key := range keys {
		if len(m[key]) >= 2 {
			r = append(r, m[key][0].Value)
		}

		if len(r) == 2 {
			break
		}
	}

	return len(r) == 2, r
}

func manualStraightCheck(cards []*card.Card) (bool, int) {
	var last *card.Card = nil
	first := -1
	counter := 0

	for idx, card := range cards {
		if last == nil {
			last = card
		}

		if card.Value+1 == last.Value {
			counter++
			if first == -1 {
				first = idx
			}
		} else {
			first = -1
			counter = 0
		}

		last = card
		if counter == 4 {
			break
		}
	}

	if counter == 4 {
		return true, first
	}

	return false, -1
}
