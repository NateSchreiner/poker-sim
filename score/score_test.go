package score

import (
	"fmt"
	"testing"

	"gitlab.com/NateSchreiner/poker-sim/card"
	"gitlab.com/NateSchreiner/poker-sim/cardset"
	"gitlab.com/NateSchreiner/poker-sim/hand"
)

type TestCase struct {
	input         []string
	result        bool
	returnedCards []string
}

// TODO:: Test that the 5 cards 'hasQuads' returns
// are the actual right cards
func Test_hasQuads(t *testing.T) {
	input := []string{"kd", "4d", "4h", "10d", "ad", "4s", "4c"}
	set := cardset.NewCardSet()
	loadCards(input, set)

	stats := ScoreHand(*set)
	if stats.Score != hand.FOUR_KIND {
		t.Fatalf("Expected %v, got=%v\n", hand.FOUR_KIND, stats.Score)
	}

	if stats.Quads != 1.0 {
		t.Fatalf("Expected stats to show one quads\n")
	}

	set.Clear()
	input = []string{"kd", "10d", "ad", "qs", "qd", "kh", "qc"}
	loadCards(input, set)
	stats = ScoreHand(*set)
	if stats.Score == hand.FOUR_KIND {
		t.Fatalf("Expected %v, got=%v\n", hand.FOUR_KIND, stats.Score)
	}

	if stats.Quads == 1.0 {
		t.Fatalf("Expected stats to show one quads\n")
	}
}

func loadCards(cards []string, set *cardset.CardSet) {
	for _, c := range cards {
		set.Add(card.FromString(c))
	}
}

// func Test_hasStraight(t *testing.T) {
// 	input := []string{"kd", "4d", "4h", "10d", "ad", "4s", "4c"}
// 	r := []string{}
// 	tc := TestCase{
// 		input:         input,
// 		result:        false,
// 		returnedCards: r,
// 	}

// 	result, msg := testScore(tc, hasStraight)
// 	if !result {
// 		t.Fatal(msg)
// 	}

// 	input = []string{"3d", "4d", "4h", "2d", "Ad", "As", "5c"}
// 	r = []string{"Ad", "2d", "3d", "4d", "5c"}
// 	tc.input = input
// 	tc.result = true
// 	tc.returnedCards = r

// 	result, msg = testScore(tc, hasStraight)
// 	if !result {
// 		t.Fatal(msg)
// 	}

// 	input = []string{"kd", "qh", "qs", "kc", "jh", "10s", "ad"}
// 	r = []string{"kd", "qs", "jh", "10s", "ad"}
// 	tc.input = input
// 	tc.returnedCards = r

// 	result, msg = testScore(tc, hasStraight)
// 	if !result {
// 		t.Fatal(msg)
// 	}
// }

// func Test_hasBoat(t *testing.T) {
// 	input := []string{"kd", "kh", "kc", "ks", "qh", "qs", "qc", "qd"}
// 	exp := []string{"kd", "kh", "kc", "qh", "qs", "qd"}

// 	tc := TestCase{
// 		input:         input,
// 		returnedCards: exp,
// 		result:        true,
// 	}

// 	res, msg := testScore(tc, hasBoat)
// 	if !res {
// 		t.Fatal(msg)
// 	}

// 	tc.input = []string{"2d", "2h", "2s", "3d", "kh", "as", "10d"}
// 	tc.result = false
// 	tc.returnedCards = []string{}

// 	res, msg = testScore(tc, hasBoat)
// 	if !res {
// 		t.Fatal(msg)
// 	}
// }

// func Test_hasFlush(t *testing.T) {
// 	// Test 1
// 	input := []string{"ad", "2d", "4c", "5h", "qd", "7s", "10d", "jd"}
// 	exp := []string{"ad", "2d", "qd", "10d", "jd"}
// 	tc := TestCase{
// 		input:         input,
// 		returnedCards: exp,
// 		result:        true,
// 	}

// 	res, msg := testScore(tc, hasFlush)
// 	if !res {
// 		t.Fatal(msg)
// 	}

// 	// test 2
// 	input = []string{"5d", "10d", "3d", "ad", "kd", "9d", "7d"}
// 	exp = []string{"ad", "kd", "10d", "9d", "7d"}
// 	tc.input = input
// 	tc.returnedCards = exp

// 	res, msg = testScore(tc, hasFlush)
// 	if !res {
// 		t.Fatal(msg)
// 	}

// 	// Test 3
// 	input = []string{"5d", "10d", "3h", "ad", "kd", "9s", "7h"}
// 	exp = []string{}
// 	tc.input = input
// 	tc.returnedCards = exp
// 	tc.result = false

// 	res, msg = testScore(tc, hasFlush)
// 	if !res {
// 		t.Fatal(msg)
// 	}
// }

// func Test_hasThreeKind(t *testing.T) {
// 	input := []string{"2s", "ad", "2d", "4c", "5h", "qd", "7s", "2c", "10d", "jd"}
// 	exp := []string{"2s", "2c", "ad", "2d", "qd"}
// 	tc := TestCase{
// 		input:         input,
// 		returnedCards: exp,
// 		result:        true,
// 	}

// 	res, msg := testScore(tc, hasThreeOfKind)
// 	if !res {
// 		t.Fatal(msg)
// 	}
// }

// func Test_hasTwoPair(t *testing.T) {
// 	input := []string{"ad", "2d", "js", "5h", "qd", "7s", "2c", "10d", "jd"}
// 	exp := []string{"2d", "2c", "js", "jd", "ad"}
// 	tc := TestCase{
// 		input:         input,
// 		returnedCards: exp,
// 		result:        true,
// 	}

// 	res, msg := testScore(tc, hasTwoPair)
// 	if !res {
// 		t.Fatal(msg)
// 	}

// }

func testScore(expected TestCase, fn func(s *cardset.CardSet) (bool, *cardset.CardSet)) (bool, string) {
	set := cardset.NewCardSet()
	loadCards(expected.input, set)

	has, cards := fn(set)
	if has != expected.result {
		return false, fmt.Sprintf("Expected %v returned %v\n", expected.result, has)
	}

	if expected.result == false {
		if cards.Len != 0 {
			return false, fmt.Sprintf("Returned cards length is not 0 even no straight. Len=%d\n", cards.Len)
		} else {
			return true, ""
		}
	}

	for _, c := range expected.returnedCards {
		car := card.FromString(c)
		if !cards.Contains(car, true) {
			return false, fmt.Sprintf("Didn't find expected card in returned cards. Search=%v\n", car)
		}
	}

	return true, ""
}
