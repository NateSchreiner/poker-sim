package deck

import (
	"gitlab.com/NateSchreiner/poker-sim/card"
	"gitlab.com/NateSchreiner/poker-sim/cardset"
)

type Deck struct {
	set   *cardset.CardSet
	Cards []card.Card
}

func CreateDeck(exclude []*cardset.CardSet) *Deck {
	suites := []card.Suit{card.HEART, card.DIAMOND, card.SPADE, card.CLUB}
	vals := []card.Value{card.TWO, card.THREE, card.FOUR, card.FIVE, card.SIX, card.SEVEN, card.EIGHT, card.NINE, card.TEN, card.JACK, card.QUEEN, card.KING, card.ACE}

	d := &Deck{
		Cards: make([]card.Card, 0),
		set:   cardset.NewCardSet(),
	}

	for _, set := range exclude {
		iter := cardset.Iter(set)
		for iter.More() {
			node := iter.Next()
			d.set.Add(node.Card)
		}
	}

	for _, v := range vals {
		for _, s := range suites {
			card := card.Card{
				Value: v,
				Suit:  s,
			}

			added := d.set.Add(&card)
			if added {
				d.Cards = append(d.Cards, card)
			}
		}
	}

	return d
}
