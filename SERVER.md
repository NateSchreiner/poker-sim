# Server Requests

### `range-calculator` request format

```
{
	"range": [
		"aa",
		"aks",
		"ako",
		"aqs",
		"a10s",
		"a8s"
	],
	"board": [
		
	]
}
```

>JSON form with `application/json` Content-Type