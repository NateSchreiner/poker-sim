package statistics

import (
	"gitlab.com/NateSchreiner/poker-sim/cardset"
	"gitlab.com/NateSchreiner/poker-sim/hand"
)

type Stats struct {
	Runs     int
	Score    hand.Hand
	Hand     *cardset.CardSet
	BestFive *cardset.CardSet

	Quads         float32
	Flush         float32
	Straight      float32
	ThreeKind     float32
	TwoPair       float32
	Pair          float32
	HighCard      float32
	FullHouse     float32
	StraightFlush float32

	FlushDraw             float32
	BdStraight            float32
	BdFlush               float32
	OpenEndedStraightDraw float32
	GutshotStraightDraw   float32
}

func Combine(first, second Stats) Stats {
	first.Runs += second.Runs
	if first.Score < second.Score {
		first.Score = second.Score
		first.BestFive = second.BestFive
	}

	first.Quads += second.Quads
	first.Flush += second.Flush
	first.Straight += second.Straight
	first.ThreeKind += second.ThreeKind
	first.TwoPair += second.TwoPair
	first.Pair += second.Pair
	first.HighCard += second.HighCard
	first.FullHouse += second.FullHouse
	first.StraightFlush += second.StraightFlush
	first.FlushDraw += second.FlushDraw
	first.BdStraight += second.BdStraight
	first.BdFlush += second.BdFlush
	first.OpenEndedStraightDraw += second.OpenEndedStraightDraw
	first.GutshotStraightDraw += second.GutshotStraightDraw
	return first
}
