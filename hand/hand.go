package hand

type Hand int

const (
	TWO_HIGH       Hand = iota // 0
	THREE_HIGH                 // 1
	FOUR_HIGH                  // 2
	FIVE_HIGH                  // 3
	SIX_HIGH                   // 4
	SEVEN_HIGH                 // 5
	EIGHT_HIGH                 // 6
	NINE_HIGH                  // 7
	TEN_HIGH                   // 8
	JACK_HIGH                  // 9
	QUEEN_HIGH                 // 10
	KING_HIGH                  // 11
	ACE_HIGH                   // 12
	PAIR                       // 13
	TWO_PAIR                   // 14
	TRIPS                      // 15
	STRAIGHT                   // 16
	FLUSH                      // 17
	FULL_HOUSE                 // 18
	FOUR_KIND                  // 19
	STRAIGHT_FLUSH             // 20
	ROYAL_FLUSH                // 21
)
