package cardset

import (
	"fmt"
	"gitlab.com/NateSchreiner/poker-sim/card"
	"testing"
)

// TODO:: Test that the highest cards come first.
// Explicitly test which cards are in set? (To make
// sure dups aren't added?)
func Test_addCards(t *testing.T) {
	cs := NewCardSet()
	cs.Add(card.FromString("kd"))
	cs.Add(card.FromString("kh"))
	cs.Add(card.FromString("kd"))
	if cs.Len != 2 {
		t.Fatalf("Two cards not present in CardSet")
	}

	cs.Add(card.FromString("Ad"))
	cs.Add(card.FromString("Ah"))
	cs.Add(card.FromString("Ac"))
	cs.Add(card.FromString("As"))
	cs.Add(card.FromString("Ad"))
	if cs.Len != 6 {
		t.Fatalf("Six cards not present in CardSet")
	}

	cs.Add(card.FromString("2h"))
	cs.Add(card.FromString("2h"))
	cs.Add(card.FromString("2h"))
	cs.Add(card.FromString("2h"))
	if cs.Len != 7 {
		t.Fatalf("Seven cards not present in CardSet")
	}

	fmt.Println("break")
}
