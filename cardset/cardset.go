package cardset

import (
	"gitlab.com/NateSchreiner/poker-sim/card"
	"golang.org/x/exp/maps"
)

// CardSet is a set that can hold an arbitary number of cards,
// and as the name "set" implies, it can not hold duplicate cards.
// Upon adding cards to the set, the CardSet will order the cards
// such that higher value cards come first.  Ace being the highest
// value card

type CardSet struct {
	Head   *Node
	Tail   *Node
	Len    int
	Legend map[card.Card]bool
}

func NewCardSet() *CardSet {
	s := &CardSet{
		Head:   nil,
		Tail:   nil,
		Len:    0,
		Legend: make(map[card.Card]bool, 0),
	}

	return s
}

func FromSlice(s []*card.Card) *CardSet {
	slice := &CardSet{
		Head:   nil,
		Tail:   nil,
		Len:    0,
		Legend: make(map[card.Card]bool, 0),
	}

	for _, card := range s {
		slice.Add(card)
	}

	return slice
}

func (cs *CardSet) Contains(card *card.Card, onlyVal bool) bool {
	if card.Value > cs.Head.Card.Value {
		return false
	}

	if cs.Tail != nil && card.Value < cs.Tail.Card.Value {
		return false
	}

	if onlyVal {
		return cs.setContainsVal(card)
	}

	return cs.setContainsCard(card)
}

func (cs *CardSet) setContainsCard(card *card.Card) bool {
	curr := cs.Head
	for curr != nil {
		if curr.Card.Value == card.Value {
			if curr.Card.Suit == card.Suit {
				return true
			}
		}
		curr = curr.next
	}

	return false
}

func (cs *CardSet) setContainsVal(card *card.Card) bool {
	curr := cs.Head
	for curr != nil {
		if curr.Card.Value == card.Value {
			return true
		}
		curr = curr.next
	}

	return false
}

func (cs *CardSet) Clear() {
	maps.Clear(cs.Legend)
	cs.Head = nil
	cs.Tail = nil
	cs.Len = 0
}

func (cs *CardSet) Empty() bool {
	return cs.Len == 0 && len(cs.Legend) == 0
}

type Iterator struct {
	set *CardSet
	cur *Node
}

func BackIter(cs *CardSet) *Iterator {
	return &Iterator{
		set: cs,
		cur: cs.Tail,
	}
}

func Iter(cs *CardSet) *Iterator {
	i := &Iterator{
		set: cs,
		cur: cs.Head,
	}

	return i
}

func (i *Iterator) More() bool {
	return i.cur != nil
}

func (i *Iterator) Prev() *Node {
	r := i.cur
	i.cur = r.prev
	return r
}

func (i *Iterator) Next() *Node {
	r := i.cur
	i.cur = r.next
	return r
}

func (i *Iterator) Peek() *Node {
	return i.cur
}

func (i *Iterator) DoublePeek() *Node {
	return i.cur.next
}

// Returns false if card is already in cardset
func (cs *CardSet) Add(v *card.Card) bool {
	_, ok := cs.Legend[*v]
	if !ok {
		// Need to add to the set
		n := newNode(v)
		if cs.Head == nil {
			cs.Head = n
		} else if n.Card.Larger(cs.Head.Card) {
			tmp := cs.Head
			tmp.prev = n
			cs.Head = n
			n.next = tmp
		} else {
			// TODO:: NEEDS TESTING
			cs.Tail = cs.Head.insert(n)
		}

		cs.Len++
		cs.Legend[*v] = true
		return true
	}

	return false
}

type Node struct {
	Card *card.Card
	next *Node
	prev *Node
}

func newNode(v *card.Card) *Node {
	n := &Node{
		Card: v,
		next: nil,
		prev: nil,
	}
	return n
}

// TODO:: NEEDS TESTING
func (n *Node) insert(v *Node) *Node {
	if n.next == nil {
		n.next = v
		v.prev = n
		return v
	} else if v.Card.Larger(n.next.Card) {
		tmp := n.next
		tmp.prev = v
		v.next = tmp
		n.next = v
		v.prev = n

		curr := tmp
		for curr.next != nil {
			curr = curr.next
		}
		return curr
	} else {
		return n.next.insert(v)
	}
}
