package main

import (
	"fmt"
	"log"
	"net/http"
	"runtime"

	"github.com/gin-gonic/gin"
	"gitlab.com/NateSchreiner/poker-sim/card"
	"gitlab.com/NateSchreiner/poker-sim/cardset"
	"gitlab.com/NateSchreiner/poker-sim/handrange"
	"gitlab.com/NateSchreiner/poker-sim/simulator"
)

func main() {
	cpu := runtime.NumCPU()
	runtime.GOMAXPROCS(cpu)
	fmt.Printf("Running with %d cpus\n", cpu)

	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.POST("/equity-calc", equityCalc)
	router.POST("/range-stats", rangeStats)
	router.GET("/test", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"Hello!": "World!"})
	})

	err := router.Run(":8080")
	if err != nil {
		fmt.Errorf(err.Error())
	}
}

type Hand struct {
	First  string `json:"firstCard"`
	Second string `json:"secondCard"`
}

type Hands struct {
	Hands []Hand   `json:"hands"`
	Board []string `json:"board"`
}

func equityCalc(c *gin.Context) {
	// Hands should be between 2 - 8 in length (inclusive)
	var hands Hands

	c.BindJSON(&hands)
	if len(hands.Hands) < 2 || len(hands.Hands) > 8 {
		log.Fatalf("Length not correct! len=%d\n", len(hands.Hands))
	}

	h := make([]*cardset.CardSet, 0)
	for _, hand := range hands.Hands {
		c := cardset.NewCardSet()
		firstCard := card.FromString(hand.First)
		secondCard := card.FromString(hand.Second)
		c.Add(firstCard)
		c.Add(secondCard)
		h = append(h, c)
	}

	b := cardset.NewCardSet()
	for _, c := range hands.Board {
		card := card.FromString(c)
		b.Add(card)
	}

	sim := simulator.NewSim(1_000_000, h, b, false)
	sim.Run()

	fmt.Printf("%v\n", hands)
	fmt.Println("EquityCalc!")
}

type album struct {
	ID     string  `json:"id"`
	Title  string  `json:"title"`
	Artist string  `json:"artist"`
	Price  float64 `json:"price"`
}

//		{
//			"albums": [
//			   {"id": "4","title": "The Modern Sound of Betty Carter","artist": "Betty Carter","price": 49.99},
//			   {"id": "5","title": "Wow, this works","artist": "NateSchreiner","price": 777.555}
//			]
//	}
type Albums struct {
	Albums []album `json:"albums"`
}

type HandRange struct {
	Range []string `json:"range"`
	Board []string `json:"board"`
}

func rangeStats(c *gin.Context) {
	var t HandRange
	// c.Bind(&t)
	c.BindJSON(&t)
	m := make(map[string][]*cardset.CardSet, 0)
	ranges := &handrange.Ranges{
		Hands: m,
	}
	cards := ranges.RangeToHands(t.Range)
	fmt.Printf("%v\n", cards)
	fmt.Println("RangeStats!")
}
