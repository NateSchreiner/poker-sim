package card

import (
	"fmt"
	"strings"
)

type Value int

const (
	TWO   Value = iota // 0
	THREE              // 1
	FOUR               // 2
	FIVE
	SIX
	SEVEN
	EIGHT
	NINE
	TEN
	JACK
	QUEEN
	KING
	ACE
)

type Suit string

const (
	HEART   Suit = "heart"
	DIAMOND Suit = "diamond"
	SPADE   Suit = "spade"
	CLUB    Suit = "club"
)

type Card struct {
	Value Value
	Suit  Suit
}

func (c *Card) Larger(o *Card) bool {
	if o == nil {
		return true
	}
	return c.Value >= o.Value
}

var cardCache map[string]*Card

func getSuitFromLetterCode(c int) Suit {

	if c >= 99 {
		// Uppercase letter
		c = c - 32
	}

	switch c {
	case 72:
		return HEART
	case 68:
		return DIAMOND
	case 83:
		return SPADE
	case 67:
		return CLUB
	default:
		panic(fmt.Sprintf("suit letter code unrecognized: %s", fmt.Sprint(c)))
	}
}

func IntToCardValue(c int) Value {
	if c == 10 {
		return TEN
	}
	return getValueFromLetterCode(c)
}

func getValueFromLetterCode(c int) Value {
	if c >= 50 && c <= 57 {
		// we have a number
		switch c {
		case 50:
			return TWO
		case 51:
			return THREE
		case 52:
			return FOUR
		case 53:
			return FIVE
		case 54:
			return SIX
		case 55:
			return SEVEN
		case 56:
			return EIGHT
		case 57:
			return NINE
		}
	}

	return getFaceCardValue(c)
}

func getFaceCardValue(c int) Value {
	if c > 96 {
		c = c - 32
	}

	switch c {
	case 65:
		return ACE
	case 75:
		return KING
	case 81:
		return QUEEN
	case 74:
		return JACK
	default:
		panic(fmt.Sprintf("card value code unrecognized: %s", fmt.Sprint(c)))

	}
}

// Used for converting "hand" string to it's specific two cards
// i.e. "Ah9h" or "10sJc" or "10js" or "A9ss"
func StringToCards(s string) []*Card {
	r := make([]*Card, 0)
	startIdx := 0
	endIdx := -1

	var curr *Card
	for i, c := range s {
		switch c {
		case 83, 115, 72, 104, 67, 99, 68, 100:
			endIdx = i
			curr = FromString(s[startIdx:endIdx])
			r = append(r, curr)
			startIdx = i + 1
		}
	}

	return r
}

// FromString given a string such as Ah (Ace of Hearts) or 7d (Seven of Diamonds)
// 10s is outlier
//
//	aH == Ah TODO :: Test
//
// returns a corresponding Card instance with correct Value and Suit
func FromString(c string) *Card {
	if cardCache == nil {
		cardCache = make(map[string]*Card)
	}

	lc := strings.ToLower(c)
	ptr, ok := cardCache[lc]
	if ok {
		return ptr
	} else {
		if len(c) > 2 {
			if len(c) != 3 {
				panic(fmt.Sprintf("unkown card format: %s", c))
			}
			if int(c[0]) != 49 || int(c[1]) != 48 {
				// ascii 1 and 0
				panic(fmt.Sprintf("unkown card format: %s", c))
			}

			suit := getSuitFromLetterCode(int(c[2]))
			r := &Card{
				Value: TEN,
				Suit:  suit,
			}

			cardCache[lc] = r
			return r
		}
		if len(c) != 2 {
			panic(fmt.Sprintf("unkown card format: %s", c))
		}

		value := getValueFromLetterCode(int(c[0]))
		suit := getSuitFromLetterCode(int(c[1]))

		r := &Card{
			Value: value,
			Suit:  suit,
		}

		cardCache[lc] = r
		return r
	}
}
