package card

import (
	"fmt"
	"testing"
)

func Test_cardCompare(t *testing.T) {
	ace_hearts := FromString("Ah")
	king_spades := FromString("kS")

	ace_spades := FromString("as")

	if ace_hearts == king_spades {
		t.Fatalf("Ace is not equal to King")
	}

	if ace_spades == ace_hearts {
		fmt.Println("Could argue for similarity")
	}

	if ace_spades == king_spades {
		t.Fatalf("Ace does not equal King : Spades")
	}

	c := FromString("aH")
	if ace_hearts != c {

		t.Fatalf("Ah does equal aH")
	}
}
