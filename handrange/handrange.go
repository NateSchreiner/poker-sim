package handrange

import (
	"fmt"
	"log"

	"gitlab.com/NateSchreiner/poker-sim/card"
	"gitlab.com/NateSchreiner/poker-sim/cardset"
)

// Cold startup would compute all the combinations, but it would add it to a shared
// map that given key 89o, it would return a slice of all the Hand

type Ranges struct {
	Hands map[string][]*cardset.CardSet
}

func NewHandRange() *Ranges {
	h := make(map[string][]*cardset.CardSet, 0)
	return &Ranges{
		Hands: h,
	}
}

type rng struct {
	first  int
	second int
	suited bool
	str    string
}

// Function takes a list of "ranges" such as: "A9s", "kjo" and converts them
// to their physical hand combinations.  "A9s" => ["Ad9d", "Ah9h", "As9s", "Ac9c"]
func (rngs *Ranges) RangeToHands(rang []string) []*cardset.CardSet {
	ret := make([]*cardset.CardSet, 0)
	for _, r := range rang {
		fmt.Printf("HandRange: %s\n", r)
		if val, ok := rngs.Hands[r]; ok {
			ret = append(ret, val...)
		} else {
			rg := rngs.rangeToCards(r)
			ret = append(ret, rg...)
		}
	}

	return ret
}

func (rngs *Ranges) rangeToCards(rang string) []*cardset.CardSet {
	isTen := false
	r := rng{
		first:  -1,
		second: -1,
		suited: false,
		str:    rang,
	}
	for _, c := range rang {
		switch c {
		case 48:
			isTen = true
		case 49:
			if !isTen {
				if r.first == -1 {
					r.first = 10
				} else {
					r.second = 10
				}
				isTen = true
			} else {
				isTen = false
			}
		case 50, 51, 52, 53, 54, 55, 56, 57, 97, 65, 107, 75, 113, 81, 106, 74:
			if r.first == -1 {
				r.first = int(c)
			} else {
				r.second = int(c)
			}
		case 115, 83:
			r.suited = true
		case 111, 79:
			r.suited = false
		}
	}

	if r.first == -1 || r.second == -1 {
		log.Fatalf("Invalid input!! %s\n", rang)
	}
	return rngs.rangeToHands(r)
}

func (rngs *Ranges) rangeToHands(r rng) []*cardset.CardSet {
	suites := []card.Suit{card.CLUB, card.DIAMOND, card.HEART, card.SPADE}
	hands := make([]*cardset.CardSet, 0)
	firstCardValue := card.IntToCardValue(r.first)
	secondCardValue := card.IntToCardValue(r.second)

	if r.suited {
		if r.first == r.second {
			panic("Paired hands can't be suited")
		}

		set := cardset.NewCardSet()
		for _, suite := range suites {
			set.Clear()
			set.Add(&card.Card{Value: firstCardValue, Suit: suite})
			set.Add(&card.Card{Value: secondCardValue, Suit: suite})
			hands = append(hands, set)
		}

		return hands
	}

	for _, sOne := range suites {
		for _, sTwo := range suites {
			if sOne == sTwo {
				continue
			}
			set := cardset.NewCardSet()
			set.Add(&card.Card{Value: firstCardValue, Suit: sOne})
			set.Add(&card.Card{Value: secondCardValue, Suit: sTwo})
			hands = append(hands, set)
		}
	}

	rngs.Hands[r.str] = hands
	return hands
}
