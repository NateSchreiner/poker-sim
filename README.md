# Poker Simulator

Given 2 - 9 hands, and either 0, 3, 4, 5 "board" cards, compute which hand(s) will win the most in the long run.

ie. Compute the percentage of time each hand wins over a long number of sims (1,000,000)

Can the computation function take a range instead of a hand?
    Gets a lot more compute heavy
        - Which range can have the STRONGEST hand? 
        - Which range has the highest chance, (most probable) to have the strongest hand


### Compute Facts

1326 Starting Hands

so (1326 x 1324) = 1.75million starting configurations of hand VS hand ?? 

## Biggest Problems
1. How to compare 3+ hands? 
    -> We actually just assign scores to hands, score each hand and compare
2. How to compute and compare ranges of hands? 
    -> do we flesh these out to actual instances of Hand and divide the work up across go-routines?  
3. How to collect statistics on a range?
    -> Do above, Flesh out to ranges, divide among go-routines which each collect their own
        stats, then combine and compute

## TODO 

- Write a hand comparison algorithm
- What will I use to hold hands? Boards? 

