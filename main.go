package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Server struct {
	http.Handler
}

type Hand struct {
	CardOne string
	CardTwo string
}

func tryOne(writer http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		fmt.Fprintf(writer, "ParseForm() err: %v", err)
		return
	}

	fmt.Fprintf(writer, "Post: %v\n", req.PostForm)
	hand := req.FormValue("Hand")
	fmt.Printf("Hand: %s", hand)

}

func tryTwo(writer http.ResponseWriter, req *http.Request) {
	//var res map[string]interface{}
	hand := &Hand{}

	json.NewDecoder(req.Body).Decode(hand)

	fmt.Println(hand)
}

func (s *Server) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	//tryOne(writer, req)
	tryTwo(writer, req)
}

func main() {

	cpus := 8
	total := 1010
	chunkSize := total / cpus
	fmt.Printf("Chunk size: %d\n", chunkSize)

	for i := 1; i <= cpus; i++ {
		start := (i - 1) * chunkSize
		end := (i * chunkSize) - 1
		fmt.Printf("Iter: %d handles: %d - %d\n", i, start, end)
	}

	// rng := "47o"
	// rngs := handrange.Ranges{}
	// set := rngs.RangeToCards(rng)
	// fmt.Printf("Length: %d\n", len(set))
	// for _, s := range set {
	// 	fmt.Print(s)
	// 	fmt.Println(" . ")
	// }
	// // var suit card.Value
	// // second := card.TWO

	// // if suit == -1 {
	// // 	fmt.Printf("CORRECT!\n")
	// // }
	// // fmt.Printf("Zero: %d\n", suit)
	// fmt.Printf("Other: %d\n", second)
	// Test_binarySearch()
	//s := sortedset.SortedSet{}
	//s.Add(card.FromString("ah"))
	//server := &Server{}
	//err := http.ListenAndServe(":1080", server)
	//if err != nil {
	//	panic(err)
	//}
}

// Returns the index of 'target' in 'arr' or -1 if not present
func binary_search(arr []int, target int) int {
	start, end := 0, len(arr)-1

	for start < end {
		mid := (start + end) / 2
		if target < arr[mid] {
			end = mid
		} else if target > arr[mid] {
			start = mid
		} else {
			return mid
		}
	}

	return -1
}

func Test_binarySearch() {
	l := []int{1, 3, 4, 5, 6, 7, 8, 9}
	target := 7

	fmt.Printf("TargetIndex: %d", binary_search(l, target))
}
