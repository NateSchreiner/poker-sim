package simulator

import (
	"runtime"
	"sync"

	"gitlab.com/NateSchreiner/poker-sim/cardset"
	"gitlab.com/NateSchreiner/poker-sim/deck"
	"gitlab.com/NateSchreiner/poker-sim/score"
	"gitlab.com/NateSchreiner/poker-sim/statistics"
)

type Simulator struct {
	rangeSim bool
	hands    []*cardset.CardSet
	board    *cardset.CardSet
	results  []*statistics.Stats
	iters    int
	cpus     int
}

func NewSim(iter int, hands []*cardset.CardSet, board *cardset.CardSet, rangeSim bool) *Simulator {
	return &Simulator{
		rangeSim: rangeSim,
		hands:    hands,
		board:    board,
		results:  make([]*statistics.Stats, 0),
		iters:    iter,
		cpus:     runtime.NumCPU(),
	}
}

func (s *Simulator) Run() {
	sets := make([]*cardset.CardSet, 0)
	sets = append(sets, s.hands...)
	sets = append(sets, s.board)
	deck := deck.CreateDeck(sets)

	if s.rangeSim {
		s.runRangeSim(deck)
	} else {
		s.runEquitySim(deck)
	}
}

// A rangeSim differs from an equity sim in the respect that during a range sim, all the
// hands contianed in s.hands will aggregate the stats together and an equitySim's stats
// will pertain to it's corresponding hand
func (s *Simulator) runRangeSim(deck *deck.Deck) {

}

func (s *Simulator) runEquitySim(deck *deck.Deck) {
	results := make(chan statistics.Stats)
	chunkSize := s.iters / s.cpus
	resMap := make(map[*cardset.CardSet]statistics.Stats, 0)
	wg := sync.WaitGroup{}

	for i := 1; i <= s.cpus; i++ {
		// start := (i - 1) * chunkSize
		// end := (i * chunkSize) - 1
		wg.Add(1)
		go s.equityHelper(&results, deck, chunkSize, &wg)
		// First chunk would be 0 - i * chunkSize
	}

	simDone := make(chan bool)

	go func() {
		for {
			select {
			case result := <-results:
				// This is the statistic from a single iteration.
				// We need to 'remember' this and track the totals across
				// entire sim.
				if val, ok := resMap[result.Hand]; !ok {
					// We need to merge these two results?
					resMap[result.Hand] = statistics.Combine(val, result)
				}
			case <-simDone:
				return
			default:
				//! This is non-blocking!
			}
		}
	}()

	wg.Wait()
	simDone <- true

}

func (s *Simulator) equityHelper(res *chan statistics.Stats, deck *deck.Deck, iters int, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < iters; i++ {
		results := score.Compare(s.hands)
		for _, result := range results {
			*res <- *result.Stats
		}
	}
}

func simulate(results chan statistics.Stats) {

}
